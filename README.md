Nextbus Target Case Study

Nextbus lets you access the location, time, and schedule of every bus 
listed on  http://svc.metrotransit.org/

Usage

You will need the following parameters:

Bus Route
Bus Stop Name
Direction
To run the program, enter the following

$ nextbus.py <"Bus Route"> <"Bus Stop Name"> <"Direction">

After running this commnd, you will receive a response of x minutes, 
where x is the number of minutes until the bus arrives at that stop.

If there is no output, the last bus for the day has already left.

Example

$ nextbus.py �METRO Blue Line� �Target Field Station Platform 1� �south�


Author
Ilhan Dahir